import logging as LOG

LOG.basicConfig(filename=f"{__package__}.log",filemode="w",encoding="utf-8",
                level=LOG.DEBUG,format="%(asctime)s:%(levelname)s:%(message)s")

def log(message: str, level="info") -> None:
    """logging interface"""
    match level.lower():
        case "error": LOG.error(message)
        case "warning": LOG.warning(message)
        case "debug": LOG.debug(message)
        case _: LOG.info(message)
