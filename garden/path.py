import os
import pathlib
import sys
import typing

def findall(pattern:str, root=pathlib.Path.cwd()
            ) -> typing.Generator[pathlib.Path]:
    """loop from root through all the files that match a pattern"""
    if root and root.is_dir() and not root.name.startswith("."):
        for p in root.rglob(pattern):
            if  p.is_file and not p.name.startswith("."): yield p

def find(candidate:pathlib.Path) -> pathlib.Path:
    """path of a candidate or first file matching its suffix"""
    return candidate if (candidate.exists()
        ) else next((i for i in findall(
            f"*{candidate.suffix}", candidate.parent)), None)

def which(name:str) -> pathlib.Path:
    """check the availability of a named program

    Consider an available program located either in the current folder
    or in the system PATH.

    alternative to shutil.which(command)
    """
    dirlist = [path for path in findall("*")]
    dirlist.extend(# add PATH entries to the search
        pathlib.Path(i) for i in os.getenv("PATH").split(os.pathsep))
    """
    available extensions for the current system
    """
    ext_nix, ext_win = ("", ".sh"), (".exe", ".bat", ".cmd")
    extensions = ext_win if "win32" in sys.platform else ext_nix
    """
    search
    """
    for basedir in dirlist:
        for ext in extensions:
            candidate = (basedir / name.lower()).with_suffix(ext)
            if candidate.exists():
                return candidate
