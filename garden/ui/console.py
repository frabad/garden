import xml.etree.ElementTree as ET

class FieldSet(object):
    """standard input fieldset drawer and reader"""
    
    def __init__(self, xml):
        self.xml = xml
        self._fields = []
        self.draw()
    
    def draw(self):
        """draw main controls and attach events"""
        
        def add_field(field, position):
            """draw a field at position"""
            label, value = field.tag, field.text
            if value and value != "":
                 label += " (default is %s)" % value
            entry = input("%s: " % label) or value
            self._fields.append(entry)
        
        print("%s : " % self.xml.tag)
        for position, field in enumerate(self.xml):
            add_field(field,position)
        self.save()
    
    def save(self):
        """update tree with fields values"""
        for i,field in enumerate(self._fields):
            self.xml[i].text=field
    
    def __str__(self):
        """make human readable"""
        return ET.tostring(self.xml, encoding="unicode")


