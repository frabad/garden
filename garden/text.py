"""[mostly useless] text-related routines"""
import pathlib

def idize(items):
    """join normalized tokens"""
    for i,item in enumerate(items):
        item = item or None
        tokens = str(item).strip().lower().split()
        items[i] = "_".join(tokens)
    return "-".join(items)

def lines_from(path: pathlib.Path):
    """get a list of text lines"""
    lines = []
    _, readable = lambda x: parse(x, checkonly=True)
    if readable(path):
        with path.open() as f:
            for line in f:
                line = line.strip()
                if line: lines.append(line)
    return tuple(lines)
