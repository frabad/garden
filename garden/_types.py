from collections.abc import MutableMapping
from . import etree
from .log import log
import pathlib
import sys
from ._sys import require
import typing

class Tree(object):
    """XML document ready for object-oriented chained processing."""

    def __init__(self, xml) -> None:
        """Initialize Document with XML content."""
        self.root, self.error = xml, None

    @classmethod
    def load(cls, source: pathlib.Path):
        """Initialize document from file {}."""
        log(cls.load.__doc__.format(source))
        xml, error = etree.parse(source)
        document = cls(xml)
        if error:
            document.error = error
            log(error,"error")
        return document

    def store(self, fp: pathlib.Path) -> bool:
        """Store document content in file {}."""
        ok, error = etree.to_file(fp,self.root)
        log(self.store.__doc__.format(fp))
        if error is None: return ok
        self.error = error
        log(error,"error")

    def transform(self, stylesheet: pathlib.Path, **params) -> typing.Self:
        """Perform an XSL transformation with stylesheet {}."""
        log(self.transform.__doc__.format(stylesheet))
        result, error = etree.transform(stylesheet,self.root,**params)
        require(error is None, error)
        self.root, self.error = etree.parse(result)
        if error: log(error,"error")
        return self # fluent API: only usecase

    def find(self, expression:str):
        """Query with simple XPath expression {}."""
        if self.root is not None:
            result = None
            try:
                result = self.root.find(expression)
            except etree.LX.LxmlError as e:
                log("Unsupported XPath expression.","warning")
            if result is not None and len(result) == 1:
                return result[0]
            return result

    def query(self, expression:str):
        """Query with advanced XPath expression {}."""
        if self.root is not None:
            return etree.query(self.root,expression)

    def validate(self, model: pathlib.Path) -> bool:
        """Validate against model {}."""
        log(self.validate.__doc__.format(model))
        error = etree.validate(model,self.root)
        valid = error is None
        if not valid:
            log(error,"error")
            self.error = error
        return valid

class Struct(MutableMapping):
    """
    A dictionary that applies an arbitrary key-altering
    function before accessing the keys and allows object-notation for
    reading them. Also sets keys, values and items as properties.

    source:
        https://stackoverflow.com/a/3387975
    """

    @classmethod
    def from_args(names) -> typing.Self:
        """a Struct of args"""

        def args(names) -> dict:
            """a dictionary of named sys arguments"""
            name = lambda x: sys.argv[x] if len(sys.argv) > x else None
            names = names.split() if isinstance(names,str) else names
            values = (name(i) for i in range(len(names)))
            return {k:v for k,v in zip(names,values)}

        return Struct(**args(names))

    def __init__(self, *args, **kwargs):
        self.store = dict()
        self.update(dict(*args, **kwargs))  # use the free update to set keys

    def __getitem__(self, key):
        return self.store[self._keytransform(key)]

    def __setitem__(self, key, value):
        k,v = self._keytransform(key), value
        self.store[k] = v
        if not(any([callable(v),type(v) is staticmethod])):
            self.__dict__[k] = v

    def __delitem__(self, key):
        k = self._keytransform(key)
        del self.store[k]
        del self.__dict__[k]

    def __iter__(self):
        return iter(self.store)

    def __len__(self):
        return len(self.store)

    def _keytransform(self, key):
        return key.lower() if isinstance(key,str) else key

    def keys(self):
        return self.store.keys()

    def values(self):
        return self.store.values()

    def items(self):
        return self.store.items()
