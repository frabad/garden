<?xml version="1.0" encoding="UTF-8" ?>
<xsl:transform version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:h="http://www.w3.org/1999/xhtml">
  
  <!-- HTML cleaner -->
  
  <xsl:output method="xml" indent="yes"/>
  <xsl:strip-space elements="*"/>
  <xsl:preserve-space elements="code pre"/>
  
  <xsl:template match="*|@*" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()" />
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="script | style | hr | br |
    div[not(attribute::*)] | span[not(attribute::class or attribute::role)]
    ">
    <!-- remove styling and scripting elements -->
  </xsl:template>
  
  <xsl:template match="@style | @align | @border">
    <!-- remove styling attributes -->
  </xsl:template>
  
  <xsl:template match="@*[normalize-space()='']">
    <!-- remove empty attributes -->
  </xsl:template>
  
  <xsl:template match="small | large | noscript">
    <!-- unwrap flow contents -->
    <xsl:apply-templates/>
  </xsl:template>
  
</xsl:transform>

