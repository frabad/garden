"""
Very High-Level Saxon Wrapper for Python.

Bundles only some basic features. Not used yet.

https://www.saxonica.com/saxon-c/documentation12/index.html#!samples/samples_python

"""
import sys
try:
    import saxonche as SXN
except:
    print("%s.\n\ninstallation\n\t: %s\ndetails\n\t: %s" % (
        "SaxonC-HE for Python is required",
        "pip install saxonche",
        "https://pypi.org/project/saxonche/"))
    sys.exit(0)
import pathlib

def parse(xml):
    """Parsing XML from a string or file"""
    with SXN.PySaxonProcessor(license=False) as proc:
        try:
            if isinstance(xml,str):
                return proc.parse_xml(xml_text=xml)
            if isinstance(xml,pathlib.Path):
                return proc.parse_xml(xml_file_name=str(xml.absolute()))
        except SXN.PySaxonApiError as e:
            pass # an error message is displayed anyway

def xpath(xml:str,expr:str):
    """https://www.saxonica.com/documentation12/index.html#!xpath-api/saxonc-xpath/python-api-xpath"""
    with SXN.PySaxonProcessor(license=False) as proc:
        xp = proc.new_xpath_processor()
        node = proc.parse_xml(xml_text=xml)
        xp.set_context(xdm_item=node)
        return xp.evaluate_single(expr)

def xslt(xsltfp:pathlib.Path,xmlfp:pathlib.Path,**params):
    """transformation using PyXsltExecutable. Not tested."""
    with SXN.PySaxonProcessor(license=False) as proc:
        print(proc.version)
        xsltproc = proc.new_xslt30_processor()
        executable = xsltproc.compile_stylesheet(stylesheet_file=str(xsltfp.absolute()))
        executable.set_initial_match_selection(file_name=str(xmlfp.absolute()))
        executable.set_result_as_raw_value(True)
        for name,value in params.items():
            #xdm_atomic_value = proc.make_integer_value(5)
            executable.set_parameter(name,value)
        return executable.apply_templates_returning_string()

if __name__ == "__main__":
    a = parse(pathlib.Path("doc"))
    if a: print(xpath(str(a),"count(//*[1]/string())"))