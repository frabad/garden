from importlib.metadata import version 
from xml.etree.ElementTree import ElementTree
from . import etree,sgml # TODO: only import interface classes here

def run():
    """Process a single document or directory."""
    print(f"{__package__} version "+version(__package__))
