import ctypes
import locale
import os
import sys


def require(condition: bool, message: str) -> None:
    """Terminate with a message if condition is not met."""
    if not condition:
        SystemExit(message)

def lang() -> str:
    """user lang"""
    lang = os.getenv('LANG')
    if "win32" in sys.platform:
        lang_code = ctypes.windll.kernel32.GetUserDefaultUILanguage()
        lang = locale.windows_locale[lang_code]
    return lang or locale.getdefaultlocale()[0]
